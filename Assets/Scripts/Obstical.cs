﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstical : MonoBehaviour
{
    public int damage = 1;
    public float speed;
    public GameObject effect;
    public GameObject audio;
    public Shake shake;
    private void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
        shake = GameObject.FindGameObjectWithTag("screenShake").GetComponent<Shake>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player")) {
            shake.camShake();
            Instantiate(audio, transform.position, Quaternion.identity);
            Instantiate(effect, transform.position, Quaternion.identity);
            other.GetComponent<Player>().helth -= damage;
            Destroy(gameObject);
        }
    }
}
