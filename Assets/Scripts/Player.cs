﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    private Vector2 targetPos;
    public float yStep = 5f;
    public float speed;
    public float maxHeight;
    public float minHeight;
    public int helth = 3;
    public GameObject effect;
    public Text hpText;
    public Shake shake;
    public GameObject pannelGameOver;

    void Start() {
        shake = GameObject.FindGameObjectWithTag("screenShake").GetComponent<Shake>();
    }
    void Update(){
        hpText.text = helth.ToString();
        if (helth <= 0)
        {
            pannelGameOver.SetActive(true);
            Time.timeScale = 0;
        }
        transform.position = Vector2.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow)) && transform.position.y < maxHeight) {
            shake.camShake();
            Instantiate(effect, transform.position, Quaternion.identity);
            targetPos = new Vector2(transform.position.x, transform.position.y + yStep);
        } else if ((Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow)) && transform.position.y > minHeight) {
            shake.camShake();
            Instantiate(effect, transform.position, Quaternion.identity);
            targetPos = new Vector2(transform.position.x, transform.position.y - yStep);
        }
    }
}
